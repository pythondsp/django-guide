# books/urls.py

from django.conf.urls import url

from .views import (    http_example,
        fexample, CExample,
        book_list, BookList, AuthorList,
        BookDetail,
        read_arg, read_kwarg, ReadArg, ReadKwarg,
        SearchBooks, QSearchBooks, BookSearch, BookSearchResult, 
        author_create, AuthorCreate, BookUpdate,
    )

# f in the link name, is used for function based view, otherwise class based view

urlpatterns = [
    url(r'^httpExample/$', http_example, name="httpExample"), # HttpResponse
    url(r'^fexample/$', fexample, name="fexample"), # function-based-view
    url(r'^cexample/$', CExample.as_view(), name="cexample"), # class-based-view

    
    url(r'^fbooklist/$', book_list, name="fbooklist"), # Book list
    url(r'^$', BookList.as_view(), name="booklist"), # Book list
    url(r'^authorlist/$', AuthorList.as_view(), name="authorlist"), # Author list


    url(r'^(?P<pk>\d+)/$', BookDetail.as_view(), name="bookdetail"), # Book list


    url(r'^freadargs/(\w+)/$', read_arg, name="freadargs"), # Read from url
    url(r'^readargs/(\w+)/$', ReadArg.as_view(), 
                                    name="readargs"), # Read from url
    url(r'^freadkwargs/(?P<test>\w+)/$', read_kwarg, 
                                    name="freadkwargs"), # Read from url
    url(r'^readkwargs/(?P<test>\w+)/$', ReadKwarg.as_view(), 
                                    name="readkwargs"), # Read from url


    url(r'^searchfromurl/(?P<urlsearch>[\w-]+)/$', SearchBooks.as_view(),
                name="searchfromurl"), # search item received from url
    url(r'^qsearchfromurl/(?P<qurlsearch>[\w-]+)/$', QSearchBooks.as_view(),
                name="qsearchfromurl"), # search item received from url using Q


    url(r'^booksearch/$', BookSearch.as_view(), 
                            name="booksearch"), #Book search
    url(r'^booksearchresult/$', BookSearchResult.as_view(), 
                                name="booksearchresult"), # Book search


    url(r'^fauthorcreate/$', author_create, 
                                    name="fauthorcreate"), # create author
    url(r'^authorcreate/$', AuthorCreate.as_view(), 
                                    name="authorcreate"), # create author

    url(r'^(?P<pk>\d+)/update/$', BookUpdate.as_view(), 
                                    name="bookupdate"), # update book
]
