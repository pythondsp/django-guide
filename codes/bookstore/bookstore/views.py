# bookstore/views.py

from django.shortcuts import render

# look template in 'root folder'
# The folder 'templates' is not checked inside the 'configuration folder'
# But it is checked inside the 'app' folder
def all_links(request):
    return render(request, 'bookstore/link_page.html')