.. _`ch_basic_setup`:

Creating project and basic setup
********************************


Introduction
============


In this chapter, we will create a simple Django project and one app i.e. 'bookstore' and 'books' receptively. In this chapter our aim it to create a basic setup for the project e.g. creating templates and different settings for local and production server etc. At the end of the chapter a model is added to the app 'books'. Then, in next chapter the various 'amdin' features are shown to manage the 'model'. 


Create project
==============

Let's create the project using following command, 

.. code-block:: shell

    $ django-admin startproject bookstore

After creating the project, we will have following directory structure which can be seen using 'tree' command

.. code-block:: text
    :linenos: 
    :emphasize-lines: 4, 6
    :caption: Directory structure after creating the project
    :name: txt_orig_dir_structure

    $ tree bookstore 

    bookstore/
    ├── bookstore
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── urls.py
    │   └── wsgi.py
    └── manage.py


.. note:: 

    * **Root folder** : The folder which contains the manage.py file is known as root-folder. 
    * **Configuration folder** : The folder which contains the 'wsgi.py' file is known as configuration-folder. 


.. _`sec_modify_configu_folderwb`: 

Modify configuration-folder
===========================

Now, we will modify the configuration-folder. And after modifying the folder we will have following folder-structure (\_\_pycache\_\_ folder is removed from the tree).  

.. code-block:: text
    :linenos: 
    :emphasize-lines: 4, 9, 11

    bookstore/
        ├── bookstore
        │   ├── __init__.py
        │   ├── settings
        │   │   ├── _development_settings.py
        │   │   ├── __init__.py
        │   │   ├── _production_settings.py
        │   │   └── settings.py
        │   ├── urls.py
        │   └── wsgi.py
        ├── db.sqlite3
        └── manage.py


Different 'production' and 'development' settings
-------------------------------------------------

For this first create a folder 'settings' inside the configuration-folder and **move the 'settings.py'** file inside it. 

Next, create two python files inside the settings-folder with names '_production_settings' and '_development_settings' for different production and local servers respectively. Both the files import the 'settings.py (e.g see Line 5 of :numref:`py_prod_server`)'. Also, the security key (Line 18) and email details (Lines 31-32) are saved in the variables, e.g. 'secret_key' etc., so that it can not been seen by others. The values of these variables are saved in the '**.bashrc**' file (see :numref:`py_bashrc`) and read by using command '**os.environ.get()**',   


* **_production_settings** : store the information for production server e.g. database, password etc. 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 5, 18, 32-33
    :caption: Production server settings
    :name: py_prod_server

    # Configuration-folder/settings/_production_settings.py
    # Add security related items in this file such as passwords and keys

    # import everything from settings.py i.e. import os etc.
    from .settings import *


    # Now overwrite necessary items

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = False
    # since DEBUG is False, therefore it is compulsory to add ALLOWED_HOSTS
    ALLOWED_HOSTS = ['*'] # add correct hostname i.e. website name


    # SECURITY WARNING: keep the secret key used in production secret!
    # Create new secret key (just add few more letters in original key)
    SECRET_KEY = os.environ.get("secret_key")


    # Database : Add production database details here
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }


    # # Email
    # EMAIL_HOST = 'smtp.gmail.com'
    # EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
    # EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_PASSWORD')
    # EMAIL_PORT = 587
    # EMAIL_USE_TLS = True


    # #crispy form settings
    # CRISPY_TEMPLATE_PACK = 'bootstrap3'


    # #django-registration-redux settings
    # ACCOUNT_ACTIVATION_DAYS=7
    # REGISTRATION_AUTO_LOGIN=True
    # SITE_ID=1
    # LOGIN_REDIRECT_URL='/'


    # SITE_HOST = 'smtp.onlinebookstore.elasticbeanstalk.com'


    # # add all sites for calling API
    # CORS_ORIGIN_ALLOW_ALL = True
    # # #API call for specific sites
    # # CORS_ORIGIN_REGEX_WHITELIST = (  ## Test these regexes somewhere - if your API won't allow AJAX calls from a whitelisted site your regexes are probably the problem
    # #     r'^https?://(.+\.)?fake.co.nz',
    # #     r'^https?://(.+\.)?fake.net.nz',
    # # )
    # # CORS_ALLOW_METHODS = (
    # #     'GET',
    # #     'OPTIONS',
    # # )





* **_development_settings** : store the information for local server e.g. database, password etc. 

.. code-block:: python
    :linenos:

    # Configuration-folder/settings/_development_settings.py
    # Add security related items in this file such as passwords and keys

    # Add this file in '.gitignore'

    # import everything from settings.py i.e. import os etc.
    from .settings import *


    # Now overwrite necessary items

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = True

    # SECURITY WARNING: keep the secret key used in production secret!
    # Create new secret key (just add few more letters in original key)
    SECRET_KEY = os.environ.get("secret_key")


    # Database : Add development database details here
    # SQLite
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }

    # # MySQL settings
    # # mysqlclient is required: pip install mysqlclient
    # # First, go to MySQL and create database
    # # e.g. 'db_mysql' using query 'create database db_mysql;'
    # DATABASES = {
    #     'default': {
    #         'ENGINE': 'django.db.backends.mysql',
    #         'NAME': 'db_mysql',
    #         'USER' : 'root',
    #         'PASSWORD' : 'root',
    #         'HOST' : '', # leave blank for localhost
    #         'PORT' : '', # leave blank for localhost
    #     }
    # }


    # # Email
    # EMAIL_HOST = 'smtp.gmail.com'
    # EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
    # EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_PASSWORD')
    # EMAIL_PORT = 587
    # EMAIL_USE_TLS = True


    # #crispy form settings
    # CRISPY_TEMPLATE_PACK = 'bootstrap3'


    # #django-registration-redux settings
    # ACCOUNT_ACTIVATION_DAYS=7
    # REGISTRATION_AUTO_LOGIN=True
    # SITE_ID=1
    # LOGIN_REDIRECT_URL='/'


    # SITE_HOST = 'smtp.onlinebookstore.elasticbeanstalk.com'


    # # add all sites for calling API
    # CORS_ORIGIN_ALLOW_ALL = True
    # # #API call for specific sites
    # # CORS_ORIGIN_REGEX_WHITELIST = (  ## Test these regexes somewhere - if your API won't allow AJAX calls from a whitelisted site your regexes are probably the problem
    # #     r'^https?://(.+\.)?fake.co.nz',
    # #     r'^https?://(.+\.)?fake.net.nz',
    # # )
    # # CORS_ALLOW_METHODS = (
    # #     'GET',
    # #     'OPTIONS',
    # # )




Next, create the \_\_init\_\_.py file inside the 'setting folder to use these two files. Lines 11-14 are used to modify the parameter of the settings.py. 

.. code-block:: python
    :linenos: 

    # Configuration-folder/settings/__init__.py 

    # Do not forget to add '_development_settings.py' in .gitignore 


    # Since '_development_settings' is in gitigonre, hence _production_settings
    # will be selected for online-server 


    # select 'development' or 'production' server
    try:
        from ._development_settings import *
    except:
        from ._production_settings import *



.. important:: 

    * Do not forget to add the '_development_settings.py' in .gitignore file, otherwise '_production_settings.py' will not work as try work will not be skipped. 



Modify settings.py to preserve original structure
-------------------------------------------------

Since, \_\_init\_\_.py is save inside the 'settings folder', therefore this folder will act as 'settings.py' whenever commands like 'from settings import \*' executed. But we need to make one change in 'settings.py' to preserve the original structure which is shown below, 

.. code-block:: python
    :linenos: 

    # Configuration-folder/settings/settings.py 

    # Build paths inside the project like this: os.path.join(BASE_DIR, ...)
    # BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    # settings.py is move one folder in, therefore add 'os.path.dirname'
    BASE_DIR = os.path.dirname(
            os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        ) 

Sensitive informations
----------------------

In :numref:`py_prod_server` we used '**os.environ.get()**' command to retried the value of the variables. These variables must be stored in the file '.bashrc' as shown below. 

.. code-block:: shell

    $ gedit ~/.bashrc


And add following lines at the end of the file, 

.. code-block:: text
    :linenos: 
    :emphasize-lines: 3
    :caption: Add sensitive informations in .bashrc
    :name: py_bashrc

    # ~/.bashrc 

    # copy key from settings.py and modify it by adding more characters to it
    export secret_key='MEHER1@f#^)9*6qd5g1jfasww)f2wg-8fkidxdsafan-99@as'

    # email settings 
    export EMAIL_HOST_USER="my_email@gmail.com"
    export EMAIL_HOST_PASSWORD="my_password"



.. _`sec_template_folder_set`:

Add template folder
-------------------

**Create a template folder** in the 'rood directory' and then add it to 'settings.py' as shown in Line 6 of below code. In this folder, we will store some common templates e.g. 'nav-bar' and footer etc. 


.. code-block:: python
    :linenos: 
    :emphasize-lines: 6

    # Configuration-folder/settings/settings.py 

    TEMPLATES = [
        {
            [...]
            'DIRS': [os.path.join(BASE_DIR, 'templates')],
            [...]
        },
    ]


.. _`add_static_media_folder`: 

Add 'static' and 'media' folder
-------------------------------

Next, **create one more folder 'assets'** in root directory and add it to 'settings.py' as shown in Line 6 of below code. This folder will store 'CSS' and 'JavaScript' etc. files in it. This code also includes 'STATIC_ROOT' and 'MEDIA_ROOT', which is used as storage folder, when the command 'python manage.py collectstatic' is executed.


.. code-block:: python
    :linenos: 

    # Configuration-folder/settings/settings.py 

    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR, 'static') # for command 'collectstatic'
    # save 'CSS' and 'JavaScript' etc. in 'assets'
    STATICFILES_DIRS = [os.path.join(BASE_DIR, 'assets')] 

    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'uploads') # save uploaded data


.. note:: 
    
    Download 'assets' folder from the repository and save it to root-folder. 


Media settings in urls.py
-------------------------

* MEDIA_ROOT need to be included in the 'urls.py (see Lines 11-14)' to view the 'media files' in DEBUG mode. Note that, we set the 'admin page' as home page at Line 10.

.. code-block:: python
    :linenos:
    :emphasize-lines: 4, 6, 10, 14-17
    :caption: Media files in debug mode
    :name: py_media_debug
    
    # Configuration-folder/urls.py

    from django.conf.urls import url
    from django.conf.urls.static import static
    from django.contrib import admin
    from django.conf import settings

    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', admin.site.urls),  # home page
    ]


    # required to see the media files in debug mode
    if settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)




Run migration and create superuser
----------------------------------

Now, our basic setup is ready. Next, run the 'migrations-commands' which will create the database. Then we can create the 'superuser'.  Finally, start the server' to check the setup as shown below, 

.. note::

    The Line 10 of :numref:`py_media_debug` will generate some warnings as we duplicated the 'admin.site.urls'. We will remove this when we add the 'apps' in the design. 

.. code-block:: shell
    :caption: Waring will be displayed
    :name: shell_warning

    $ python manage.py makemigrations
    [warnings ...]
    No changes detected


    $ python manage.py migrate
    [warnings ...]
    Operations to perform:
      Apply all migrations: admin, auth, contenttypes, sessions
    Running migrations:
      Applying contenttypes.0001_initial... OK
      Applying auth.0001_initial... OK
      Applying admin.0001_initial... OK
      Applying admin.0002_logentry_remove_auto_add... OK
    [...]

    $ python manage.py createsuperuser
    [warnings ...]
    Username (leave blank to use 'meher'): meher
    Email address: 
    Password: ffffjjjj
    Password (again): ffffjjjj
    Superuser created successfully.


    $ python manage.py runserver
    [...]
    Starting development server at http://127.0.0.1:8000/

Open the link 'http://127.0.0.1:8000/' and we can see the 'admin page'. 


Create base-templates
=====================

Now, we will create some base-HTML-templates for our project, and save it to folder 'templates' (settings for this was created in :numref:`sec_template_folder_set`). The html-files in the folder 'templates' will be used by rest of the html-files. 

We will have following folder structure after completing this section, 

.. code-block:: text

    bookstore/
    ├── assets
    │   ├── css
    │   │   ├── base.css
    │   │   ├── bootstrap.min.css
    │   │   ├── bootstrap-theme.min.css
    │   │   ├── font-awesome.css
    │   │   ├── footer-navbar.css
    │   │   ├── mycss.css
    │   │   └── templatemo-style.css
    │   ├── fonts
    │   │   ├── FontAwesome.otf
    │   │   ├── fontawesome-webfont.eot
    │   │   ├── fontawesome-webfont.svg
    │   │   ├── fontawesome-webfont.ttf
    │   │   ├── fontawesome-webfont.woff
    │   │   ├── Roboto-Bold-webfont.eot
    │   │   ├── Roboto-Bold-webfont.svg
    │   │   ├── Roboto-Bold-webfont.ttf
    │   │   ├── Roboto-Bold-webfont.woff
    │   │   ├── Roboto-Light-webfont.eot
    │   │   ├── Roboto-Light-webfont.svg
    │   │   ├── Roboto-Light-webfont.ttf
    │   │   ├── Roboto-Light-webfont.woff
    │   │   ├── Roboto-Regular-webfont.eot
    │   │   ├── Roboto-Regular-webfont.svg
    │   │   ├── Roboto-Regular-webfont.ttf
    │   │   └── Roboto-Regular-webfont.woff
    │   └── js
    │       ├── bootstrap.min.js
    │       ├── holder.js
    │       ├── jquery-1.11.3.js
    │       └── jquery.js
    ├── bookstore
    │   ├── __init__.py
    │   ├── settings
    │   │   ├── _development_settings.py
    │   │   ├── __init__.py
    │   │   ├── _production_settings.py
    │   │   └── settings.py
    │   ├── urls.py
    │   ├── views.py
    │   └── wsgi.py
    ├── db.sqlite3
    ├── manage.py
    └── templates
        ├── base.html
        ├── bookstore
        │   └── link_page.html
        ├── footer.html
        └── navbar.html

Add view and url
----------------

First we will create a 'view' and then add it to url.py as shown in this section, 

* **Create view**


.. code-block:: python
    :linenos:

    # bookstore/views.py

    from django.shortcuts import render

    # look template in 'root folder'
    # The folder 'templates' is not checked inside the 'configuration folder'
    # But it is checked inside the 'app' folder
    def all_links(request):
        return render(request, 'bookstore/link_page.html')

* *Add template for views.py**


Note that the folder 'templates' is checked inside the 'apps' but not in the 'configuration folder'. But we have added the custom location for the folder 'templates' in :numref:`sec_template_folder_set`; and we will use this folder to save the html-files of 'configuration folder'. 

For this create the folder template in the 'root folder'.  Then create an html file 'bookstore/link_page.html' inside it, 

.. code-block:: html

    <h1> Links </h1>

    <p>This page will contain all links</p>

    <p class="btn btn-info">Check Bootstrap 'btn-class' </p>

    <br><br>
    <p><span class="fa fa-list-alt"></span> Check Font-awe-some icon <span class="fa fa-bullhorn"></p>


* **Modify url.py** 

.. code-block:: python
    :linenos:
    :emphasize-lines: 8, 13

    # Configuration-folder/urls.py

    from django.conf.urls import url
    from django.conf.urls.static import static
    from django.contrib import admin
    from django.conf import settings

    from .views import all_links 


    urlpatterns = [
        url(r'^admin/', admin.site.urls),
        url(r'^$', all_links, name="home"),  # home page
    ]


    # required to see the media files in debug mode
    if settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


* Now run the server again, and warning of :numref:`shell_warning` will be removed as there is not duplication now, 

.. code-block:: shell

    $ python manage.py runserver


* Next open the link 'http://127.0.0.1:8000/' and we will see the page as shown in :numref:`fig_without_css`, 


.. _`fig_without_css`:

.. figure:: fig/basic/fig_without_css.png

   Home page



.. _`sec_add_template`: 

Add templates
-------------

Now we will add the base-templates to improve the look of the html-pages. For this we need to add following files, 


* **'base.html'** : Style provided in this template will be used by all the templates. 

.. code-block:: html
    :linenos: 
    :emphasize-lines: 21, 26, 32, 34, 49, 53
    :caption: base.html
    :name: html_base_html

    <!-- templates/base.html  -->

    <!DOCTYPE html>
    <html>
    <head>
        <title> Bookstore </title>

        {% load staticfiles %}

        <!-- Bootstrap core CSS -->
        <!-- Bootstrap core CSS -->
        <link href="{% static 'css/bootstrap.min.css' %}" rel="stylesheet">
        <link href="{% static 'css/bootstrap-theme.min.css' %}" rel="stylesheet">
        <link href="{% static 'css/font-awesome.css' %} "  rel="stylesheet"/>
        <link href="{% static 'css/footer-navbar.css' %} "  rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="{% static 'css/base.css' %}" rel="stylesheet">
        <link href="{% static 'css/mycss.css' %}" rel="stylesheet">

        <!-- custom stle -->
        {% block style %} {% endblock %}
    </head>
    <body>

        <!-- add navigation bar  -->
        {% include 'navbar.html' %}



        <div class="container-fluid">
          <div class="row">
            {% block jumbotron %} {% endblock %}

            {% block content %} {% endblock %}
            </div>
        </div>


        <!-- make some spacing between content and footer. -->
        <br><br>

        

        <!-- jQurery -->
        <script src="{% static 'js/jquery.js' %}"></script>
        <script src="{% static 'js/bootstrap.min.js' %}"></script>
        <script src="{% static 'js/holder.js' %}"></script>
        <!-- add custom jQurery -->
        {% block jquery %} {% endblock %}

        
        <!-- footer -->
        {% include 'footer.html' %}

    </body>
    </html>

* **'navbar.html'** : This is used by Line 26 of :numref:`html_base_html`. 


.. code-block:: html
    :linenos: 
    :caption: navbar.html
    :name: html_nabar

    <!-- templates/navbar.html  -->

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="nav navbar-brand" href="/">Bookstore</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <!-- <li><a href="/books">All Books</a></li> -->

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li class="dropdown-header">Nav header</li>
                  <li><a href="#">Separated link</a></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>



* **'footer.html'** : It can be used for adding footers and comments section. Since comments required some JavaScript codes therefore it is included below the 'jQuery' files in Line 53 of :numref:`html_base_html`.

.. code-block:: html
    :linenos: 
    :caption: footer.html
    :name: html_footer

    <!-- templates/footer.html -->

    {% load staticfiles %}


    <footer class="footer pull-right">
        <p style="font-size: 10px"> <i>Created by : Meher Krishna Patel</i></p> 
    </footer>


.. note:: 

    These templates are using various files which are saved inside the folder 'assets', which is discussed in :numref:`add_static_media_folder`. Please download this folder from the repository. 


* Finally modify the 'all_links.html' as below; and open the link 'http://127.0.0.1:8000/' which will display the page as shown in :numref:`fig_with_css` 

.. code-block:: html

    <!-- templates/bookstore/all_links.html -->

    {% extends "base.html" %}
    {% load staticfiles %}

    {% block content %}

        <h1> Links </h1>

        <p>This page will contain all links</p>

        <p class="btn btn-info">Check Bootstrap 'btn-class' </p>

        <br><br>
        <p><span class="fa fa-list-alt"></span> Check Font-awe-some icon <span class="fa fa-bullhorn"></p>

    {% endblock %}


.. _`fig_with_css`: 

.. figure:: fig/basic/fig_with_css.png

   Home page after adding Bootstrap and jQuery files etc.


App 'book'
==========


Create app
----------

Create an app 'books' as below, 

.. code-block:: shell

    $ python manage.py startapp books

Next, add it to 'settings.py', 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 11-12

    # settings.py 

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',

        # my app
        'books', 
    ]




Create model
------------

* Add following code in models.py

.. code-block:: python
    :linenos:

    # books/models.py

    from django.db import models
    from django.utils.text import slugify
     

    class Author(models.Model):
        first_name = models.CharField(max_length=50)
        middle_name = models.CharField(max_length=50, blank=True, null=True)
        last_name = models.CharField(max_length=50)
        email = models.EmailField()

        def __str__(self):
            return "%s, %s" % (self.last_name, self.first_name)

    class Publisher(models.Model):
        name    = models.CharField(max_length=50)
        address = models.CharField(max_length=50, blank=True, null=True)
        city    = models.CharField(max_length=50, blank=True, null=True)
        country = models.CharField(max_length=50, blank=True, null=True)
        website = models.URLField()


    class Book(models.Model):
        title       = models.CharField(max_length=100)
        slug        = models.SlugField(unique=True)
        authors     = models.ManyToManyField(Author)
        publisher   = models.ForeignKey(Publisher)
        content     = models.TextField()
        draft       = models.BooleanField(default=True)
        publication_date = models.DateField(null=True, blank=True)
        # verbose_name can be defined in following two ways
        timestamp   = models.DateTimeField( auto_now=False, 
                                            auto_now_add=True,
                                            verbose_name = "Created on"
                                        )
        updated     = models.DateTimeField( "Last updated", 
                                            auto_now=True, 
                                            auto_now_add=False
                                        )
        def __str__(self):
            return self.title


    # upload location : upload/book/title/slug.jpg
    def upload_location(instance, filename):
        title = slugify(instance.book.title) # Meher Baba -> Meher-Baba
        slug = instance.book.slug
        name, extension = filename.split(".")
        new_name = "%s.%s" % (slug, extension)
        return "book/%s/%s" % (title, new_name)

    class BookImage(models.Model):
        book    = models.ForeignKey(Book)
        image   = models.ImageField(upload_to=upload_location, 
                                    null=True, 
                                    blank=True
                                )

        def __str__(self): # self.image is location
            return "%s (%s)" % (self.book.title, self.image)


* Now run the migrations command to create the table in the database, 

.. code-block:: shell

    $ python manage.py makemigrations
    $ python manage.py migrate 



Register model to admin page
----------------------------

Now, we can register the model to the admin page as shown below. The 'admin' page is shown in :numref:`fig_basic1` and :numref:`fig_basic2`. 

.. code-block:: python
    :linenos:
    
    # books/admin.py

    from django.contrib import admin

    # Register your models here.
    from .models import Author, Book, BookImage, Publisher

    admin.site.register(Author)
    admin.site.register(Book)
    admin.site.register(BookImage)
    admin.site.register(Publisher)


.. _`fig_basic1`:

.. figure:: fig/basic/basic1.png

   Admin Page

.. _`fig_basic2`:

.. figure:: fig/basic/basic2.png

   Add book page
 



Conclusion
==========

In this chapter, we have created different settings for 'local' and 'production' server. Also, we have created the basic templates which will be used in next chapters. Finally a model is added in the app 'book' and register it to 'admin' page. In the next chapter, we will modify the 'admin' page to enhance the manageability of the data using 'admin-interface'. 