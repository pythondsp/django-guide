Deployment
**********


Introduction
============


In this chapter, we will learn to deploy the Django project at PythonAnywhere and Heroku. Both services have free plans for the hobbyist. 

.. important:: 

    It is better to change the name 'admin/' to something-else (e.g. admin195 etc.) in the 'urls.py', so that the others can not discover the amdin page. 


PythonAnywhere
==============

Deployment on the PythonAnywhere is straightforward. Follow the below steps to deploy our current project at the PythonAnywhere. 

.. note:: 

    * For free account, the username will the website name i.e. "username.pythonanywhere.com". 
    * One username can contain only one project in free services. 
    * But, we can create multiple accounts with single email-id. 

Create account
--------------

* Create beginner account at `PythonAnywhere <https://www.pythonanywhere.com/>`_. 

Clone project and create virtualenv
-----------------------------------

* Go to "**console**" and select **bash** as shown in :numref:`fig_deploy1`

.. _`fig_deploy1`:

.. figure:: fig/deploy/fig_deploy1.png

   Start bash



* It will open the console and type following commands there, 



.. code-block:: shell

    (check your present directory)
    $ pwd
    /home/djangoguide    # djangoguide is the username here


* Next, clone the project from Git-repository (or zip the project and upload directly on the PythonAnywhere, and the unzip it there). It is better to clone the repository than uploading the project. See `Git Guide <http://gitguide.readthedocs.io/en/latest/index.html>`_ to learn the Git-commands. 

.. note:: 

    In this section, we are creating 'cloning git-repo' and 'virtualenv' inside the folder 'djangoguide'. 


.. code-block:: shell

    $ git clone https://bitbucket.org/pythondsp/django-guide


* Now, create the virtual environment at pythonanywhere, and activate it. 

.. code-block:: shell

    $ virtualenv env -p python3.6
    $ source env/bin/activate

* Next, go to the folder where requirements.txt is saved and run the below command, 

.. note:: 

    The 'requirements.txt' files has following contents, 

    .. code-block:: text
    
        Django==1.11.10
        django-crispy-forms==1.7.0
        django-registration-redux==2.2
        Pillow==5.0.0
        pytz==2017.3


.. code-block:: shell

    $ cd path/to/requirements.txt
    
    $ pip install -r requirements.txt


Create new app
--------------

* Go to **Web**  and click on **Add a new web app**, as shown in :numref:`fig_deploy2`,

.. _`fig_deploy2`:

.. figure:: fig/deploy/fig_deploy2.png

   Create new app

* After that it will ask for some choices, select them as below. After following these steps, a message "All done ..." will be displayed as shown in :numref:`fig_deploy3`

.. code-block:: text

    * Create new web app : Click next
    * Select a Python Web framework : Manual configuration
    * Select python version : 3.6

.. _`fig_deploy3`:

.. figure:: fig/deploy/fig_deploy3.png

   New app created


Web settings
------------

.. warning:: 

    In this section, we need to provide locations of various directories. Therefore, see the directory structure carefully before filling the entries. And if something went wrong in this part, check for the  entries of the directories. 


WSGI configuration
^^^^^^^^^^^^^^^^^^

Click on the link for the WSGI configuration as shown in :numref:`fig_deploy4`, and change the code as shown below, 

* Comment the Lines 34-47 which are related to web projects using Python. 
* Uncomment the Lines 74-95 which are related to Django Project. 

    - Line 81 contains the location of the root-folder i.e. the folder which contains manage.py. The current project contains the root-folder at '/home/djangoguide/django-guide/codes/bookstore'. 
    - Line 85 is the location of 'settings.py' file. For this project, it is the location of the folder 'settings' which is working as 'settings.py' (see :numref:`sec_modify_configu_folderwb` for more details). 
    - Lines 86-88 are the values for the variables which are stored in '_production_settings.py' (see :numref:`sec_modify_configu_folderwb` for more details). 

.. code-block:: python
    :linenos: 
    :emphasize-lines: 38-47, 74-95

    # This file contains the WSGI configuration required to serve up your
    # web application at http://djangoguide.pythonanywhere.com/
    # It works by setting the variable 'application' to a WSGI handler of some
    # description.
    #

    # +++++++++++ GENERAL DEBUGGING TIPS +++++++++++
    # getting imports and sys.path right can be fiddly!
    # We've tried to collect some general tips here:
    # https://www.pythonanywhere.com/wiki/DebuggingImportError


    # +++++++++++ HELLO WORLD +++++++++++
    # A little pure-wsgi hello world we've cooked up, just
    # to prove everything works.  You should delete this
    # code to get your own working.


    HELLO_WORLD = """<html>
    <head>
        <title>Python Anywhere hosted web application</title>
    </head>
    <body>
    <h1>Hello, World!</h1>
    <p>
        This is the default welcome page for a
        <a href="https://www.pythonanywhere.com/">PythonAnywhere</a>
        hosted web application.
    </p>
    <p>
        Find out more about how to configure your own web application
        by visiting the <a href="https://www.pythonanywhere.com/web_app_setup/">web app setup</a> page
    </p>
    </body>
    </html>"""


    # def application(environ, start_response):
    #     if environ.get('PATH_INFO') == '/':
    #         status = '200 OK'
    #         content = HELLO_WORLD
    #     else:
    #         status = '404 NOT FOUND'
    #         content = 'Page not found.'
    #     response_headers = [('Content-Type', 'text/html'), ('Content-Length', str(len(content)))]
    #     start_response(status, response_headers)
    #     yield content.encode('utf8')


    # Below are templates for Django and Flask.  You should update the file
    # appropriately for the web framework you're using, and then
    # click the 'Reload /yourdomain.com/' button on the 'Web' tab to make your site
    # live.

    # +++++++++++ VIRTUALENV +++++++++++
    # If you want to use a virtualenv, set its path on the web app setup tab.
    # Then come back here and import your application object as per the
    # instructions below


    # +++++++++++ CUSTOM WSGI +++++++++++
    # If you have a WSGI file that you want to serve using PythonAnywhere, perhaps
    # in your home directory under version control, then use something like this:
    #
    #import sys
    #
    #path = '/home/djangoguide/path/to/my/app
    #if path not in sys.path:
    #    sys.path.append(path)
    #
    #from my_wsgi_file import application


    # +++++++++++ DJANGO +++++++++++
    # To use your own django app use code like this:
    import os
    import sys

    # assuming your django settings file is at '/home/djangoguide/mysite/mysite/settings.py'
    # and your manage.py is is at '/home/djangoguide/mysite/manage.py'
    path = '/home/djangoguide/django-guide/codes/bookstore'
    if path not in sys.path:
        sys.path.append(path)

    os.environ['DJANGO_SETTINGS_MODULE'] = 'bookstore.settings'
    os.environ['secret_key'] = 'adf987458745874sdfjadf'
    os.environ['EMAIL_HOST_USER'] = 'youremail@gmail.com'
    os.environ['EMAIL_PASSWORD'] = '12345'

    # then, for django >=1.5:
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()
    ## or, for older django <=1.4
    #import django.core.handlers.wsgi
    #application = django.core.handlers.wsgi.WSGIHandler()



    # +++++++++++ FLASK +++++++++++
    # Flask works like any other WSGI-compatible framework, we just need
    # to import the application.  Often Flask apps are called "app" so we
    # may need to rename it during the import:
    #
    #
    #import sys
    #
    ## The "/home/djangoguide" below specifies your home
    ## directory -- the rest should be the directory you uploaded your Flask
    ## code to underneath the home directory.  So if you just ran
    ## "git clone git@github.com/myusername/myproject.git"
    ## ...or uploaded files to the directory "myproject", then you should
    ## specify "/home/djangoguide/myproject"
    #path = '/home/djangoguide/path/to/flask_app_directory'
    #if path not in sys.path:
    #    sys.path.append(path)
    #
    ## After you uncomment the line below, the yellow triangle on the left
    ## side in our in-browser editor shows a warning saying:
    ##     'application' imported but unused.
    ## You can ignore this error. The line is necessary, and the variable
    ## is used externally.
    #from main_flask_app_file import app as application
    #
    # NB -- many Flask guides suggest you use a file called run.py; that's
    # not necessary on PythonAnywhere.  And you should make sure your code
    # does *not* invoke the flask development server with app.run(), as it
    # will prevent your wsgi file from working.


    
.. _`fig_deploy4`:

.. figure:: fig/deploy/fig_deploy4.png

   'WSGI configuration' and 'Virtual environment'


Virtual environment
^^^^^^^^^^^^^^^^^^^

Provide the location of virtualenv (i.e. /home/djangoguide/env/) as shown in :numref:`fig_deploy4`. 

Reload the webpage
^^^^^^^^^^^^^^^^^^

Now, press Reload button as shown in :numref:`fig_deploy5`. This button should be pressed, whenever we change anything in out project. 


.. _`fig_deploy5`:

.. figure:: fig/deploy/fig_deploy5.png

   Reload the webpage


Open web page
^^^^^^^^^^^^^

* Now, our basic settings are completed and we can open the webpage i.e. 'http://djangoguide.pythonanywhere.com/'

* It should open correctly but without any CSS and jQuery, as we did not provide the path for 'static' and 'media' files. 
 

Static and media files
----------------------

.. note:: 

    In our current project we saved the '**static** (i.e. Bootstrap and jQuery)' and '**media** (i.e uploaded items)' files in the '**assets**' and '**uploads**' folders respectively. 


* Look for the locations of the 'assets' and 'uploads' folders at the pythonanywhere by clicking on the '**Files**' button at the top, and filled it as shown in :numref:`fig_deploy6`. 

* Now, press Reload button as shown in :numref:`fig_deploy5` and see the web page again. Bootstrap templates should work now.


.. _`fig_deploy6`:

.. figure:: fig/deploy/fig_deploy6.png

   Load the static and media files



Static files for admin page
---------------------------

* Now, go to admin page and we will find that the static files for admin page is missing. 
* To solve this problem, go to console and type below command 

.. note::

    If 'collectstatic' command does not work due to error "SECRET_KEY can not be empty". Then, run the 'export' command, as shown below, 

.. code-block:: shell

    $ python manage.py collectstatic

    error
    [...]
    SECRET_KEY can not be empty

    $ export secret_key="adsfuoj2o3vczxu023"
    $ python manage.py collectstatic


* In our settings.py file, we set the 'STATIC_ROOT' as 'static', therefore the 'collectstatic' command will save all the files inside the folder 'static' in the root-folder. 

* Look for the location of the folder 'static' from the 'Files' tab, and save the location as shown in :numref:`fig_deploy7`. 

.. _`fig_deploy7`:

.. figure:: fig/deploy/fig_deploy7.png

   Run 'collectstatic' and save the location of collected-files

* Now, press Reload button as shown in :numref:`fig_deploy5` and see the web page again. Everything should work now. 
  
Check email setup
-----------------

Try to register a user. If email is not sent and '**500 (server error)**' is shown, it means that the 'gmail client' is not set for 'third party apps'. To resolve the problem, enable the 'third party access' for gmail account.   


MySQL database
--------------

Currently, we are using 'SQLite' database for our project. We can also use the 'MySQL' database which is available on the PythonAnywhere website. 


.. note::

    We can make these changes directly at PythonAnywhere as well. 

MySQL client
^^^^^^^^^^^^

* Intall 'mysqlclient' as it is required to connect with 'MySQL'. Next, add it in the 'requirements.txt'


.. code-block:: text

    (bookstore) $ pip install mysqlclient
    (bookstore) $ pip freeze > requirements.txt

    (bookstore) $ cat requirements.txt 
    
    Django==1.11.10
    django-crispy-forms==1.7.0
    django-registration-redux==2.2
    mysqlclient==1.3.12
    Pillow==5.0.0
    pytz==2017.3


Create and delete database
^^^^^^^^^^^^^^^^^^^^^^^^^^

Go to 'Database' tab of 'PythonAnywhere' and 'initialize' the MySQL database as shown in :numref:`fig_deploy8`. After this, a database will be create as shown in :numref:`fig_deploy9`. Note that, we can create more databases using 'create' option in ':numref:`fig_deploy9`'

.. _`fig_deploy8`:

.. figure:: fig/deploy/fig_deploy8.png

   initialize MySQL database


.. _`fig_deploy9`:

.. figure:: fig/deploy/fig_deploy9.png

   New MySQL database created


* To delete the database use following command in 'PythonAnywhere-bash',

.. code-block:: shell

    $ drop database nameOfDatabase;


Update database settings in _production_settings.py
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


Next, update the database settings for MySQL using values in :numref:`fig_deploy9`,  

.. code-block:: python

    # Database : Add production database details here
    # DATABASES = {
    #     'default': {
    #         'ENGINE': 'django.db.backends.sqlite3',
    #         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    #     }
    # }

    # MySQL settings
    # mysqlclient is required: pip install mysqlclient
    # First, go to MySQL and create database
    # e.g. 'db_mysql' using query 'create database db_mysql;'
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql', # do not change it
            'NAME': 'database_name',  # add database_name
            'USER' : 'user_name',  # add user_name
            'PASSWORD' : 'password_here',  # add password_here
            'HOST' : 'host_name', # add host_name
            'PORT' : '', # leave blank
        }
    }



Push and clone repository
^^^^^^^^^^^^^^^^^^^^^^^^^

Commit changes and push the new settings to online "Git client". Next, clone it to 'PythonAnywhere'. 

.. code-block:: shell

    (bookstore) $ git add --all
    (bookstore) $ git commit -m "MySQL settings"


Migration command
^^^^^^^^^^^^^^^^^

Finally run the migration commands at 'PythonAnywhere-bash' to create the tables in the database. 

.. code-block:: shell

    $ python manage.py makemigrations 
    $ python manage.py migrate
    $ python manage.py collectstatic
    $ python manage.py createsuperuser



Heroku
======


Deployment at Heroku is also straightforward forward, but we need to add and modify certain files in our project, 


.. warning:: 

    Media files (i.e uploaded files) will not work with following setup, 'Heroku' does not store these files. 


    To store the files, we need to activate 'Heroku's AWS S3" settings. Please see the Heroku website to activate it. 


Installation and create account
-------------------------------

First create an account at Heroku. Also, install the Heroku on the system. 


Dependencies
------------

We need four extra packages for the Heroku, 

* dj-database-url 
* gunicorn 
* whitenoise 
* psycopg2


* First, install above  packages in the virtual environment, and then update the requirements.txt file; or manually add the packages in the requirements.txt file.

.. note::  

    Below are the contents of the requirements.txt file, 
    
    .. code-block:: text
    
        dj-database-url==0.4.2
        Django==1.11.10
        django-crispy-forms==1.7.0
        django-registration-redux==2.2
        gunicorn==19.7.1
        Pillow==5.0.0
        psycopg2==2.7.4
        pytz==2017.3
        whitenoise==3.3.1



.. code-block:: shell

    (bookstore) $ pip install dj-database-url gunicorn whitenoise psycopg2
    (bookstore) $ pip freeze > requirements.txt


Create git repository in the root folder
----------------------------------------


.. warning:: 

    Create a 'git repository'  inside a 'git repository' will have undesired results. More specifically, the outer git repository will contain the items till the next git repository. 

    Therefore it is better to clone the repository and the upload it on Heroku.



* We need to create a 'git' repository in the 'root folder' where the 'manage.py' exists. 

.. note::

    Unlike PythonAnywhere setup, it is necessary to create the 'git' repository in the root-folder in Heroku.



.. code-block:: shell

    $ git init

    (next copy the .gitignore file)

* Next, copy the 'gitignore' file in the 'root-folder'. 


Create app at Heroku
--------------------

Heroku app can be create from the website or through terminal. First login and then create the app as below, 

.. code-block:: shell

    $ heroku login
    $ heroku create djangoguide


_production_settings.py
------------------------

Heroku uses the 'Postgre database", therefore add below lines in the _production_settings.py, 

.. note:: 

    Do not comment 'SQLite' database settings in 'settings.py'. Below code will update the 'default' settings of that database. 

.. code-block:: python

    # _production_settings.py

    [...]

    import dj_database_url

    db_from_env = dj_database_url.config(conn_max_age=500)
    DATABASES['default'].update(db_from_env)



wsgi.py
-------

Go to wsgi.py file in the configuration-folder, and add below content to it, 

.. code-block:: python

    # wsgi.py

    [...]

    from whitenoise.django import DjangoWhiteNoise
    application = DjangoWhiteNoise(application)



New files
---------

Next, we need to create two files in the root-folder, 

.. warning:: 

    Replace 'bookstore' with correct name configuration-folder name i.e. the folder which contains the 'wsgi.py' file.

.. code-block:: text

    $ echo "python-3.6.2" > runtime.txt 

    (replace bookstore with the project name)
    $ echo "web: gunicorn bookstore.wsgi --log-file -" > Procfile


Values for the variable
-----------------------

Now, run the below commands with corret details to store the values of the variables which are used in '_production_settings.py'

.. code-block:: shell

    $ heroku config:set secret_key="asdf4e654432&^*%23" 

    $ heroku config:set EMAIL_HOST_USER="youremail@gmail.com"

    $ heroku config:set EMAIL_PASSWORD="1234556"



Add changes to git
------------------


.. code-block:: shell

    $ git add . 
    $ git commit -m "Heroku settings"


Push to heroku and see website
------------------------------

The '**heroku create djangoguide**' creates the 'app' and automatically run the command '**git remote add heroku https://git.heroku.com/djangoguide.git**'. Therefore we are ready to push to code the 'Heroku'. 


.. note:: 

    We need not to create a "Github" or "Bitbucket" repository for this. 


* If deployment is successful, then the 'link name' with 'deployed to Heroku' will be shown in the terminal, 

.. code-block:: shell

    $ git push heroku master


    [...]
    remote:        https://djangoguide3.herokuapp.com/ deployed to Heroku
    remote: 
    remote: Verifying deploy... done.
    [...]





* Scale the project and open it, 

.. code-block:: shell

    $ heroku ps:scale web=1
    $ heroku open





Create tables in the database
-----------------------------

Finally create the tables and superuser in the newly created database, 

.. code-block:: shell

    $ heroku run python manage.py migrate
    $ heroku run python manage.py createsuperuser


Conclusion
==========

In this chapter, we learn to deploy the website on two different services i.e. PythonAnywhere and Heroku. 